package com.example.olio_teht;

import android.os.StrictMode;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class SPStorage {
    private ArrayList<SmartPost> SPList = new ArrayList<SmartPost>();
    private ArrayList<SmartPost> FilteredSPList = new ArrayList<SmartPost>();
    private static SPStorage instance = null;


    public SPStorage(){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        readXML("https://iseteenindus.smartpost.ee/api/?request=destinations&country=EE&type=APT");
        readXML("https://iseteenindus.smartpost.ee/api/?request=destinations&country=FI&type=APT");
    }

    public static SPStorage getInstance() {
        if (instance == null) {
            instance = new SPStorage();
        }
        return instance;
    }

    public void readXML(String url) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        try {
            Document document = builder.parse(url);
            document.getDocumentElement().normalize();
            //System.out.println("Root element: "+document.getDocumentElement().getNodeName());
            NodeList nList = document.getElementsByTagName("item");

            for (int i=0; i<nList.getLength();i++) {
                Node nNode = nList.item(i);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) nNode;
                    String id = element.getElementsByTagName("place_id").item(0).getTextContent();
                    String city = element.getElementsByTagName("city").item(0).getTextContent();
                    String address = element.getElementsByTagName("address").item(0).getTextContent();
                    String country = element.getElementsByTagName("country").item(0).getTextContent();
                    String postal = element.getElementsByTagName("postalcode").item(0).getTextContent();
                    String availability = element.getElementsByTagName("availability").item(0).getTextContent();
                    String description = element.getElementsByTagName("description").item(0).getTextContent();
                    if(element.getElementsByTagName("country").item(0).getTextContent().equals("FI")) {
                        String nameUnParsed = element.getElementsByTagName("name").item(0).getTextContent();
                        String partToRemove = "Posti Parcel Locker, ";
                        String replacement = "";
                        String name = nameUnParsed.replace(partToRemove,replacement);
                        addSP(id,name,city,address,country,postal,availability,description);
                    } else {
                        String name = element.getElementsByTagName("name").item(0).getTextContent();
                        addSP(id,name,city,address,country,postal,availability,description);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    public void addSP(String id, String name, String city, String add, String country, String postal, String availability, String desc) {
        SPList.add(new SmartPost(id,name,city,add,country,postal,availability,desc));
    }


    public int filterResultsByCountry (String country) {
        if (country.equals("Suomi")) {
            country = "FI";
        } else if (country.equals("Viro")) {
            country = "EE";
        } else {
            FilteredSPList.addAll(SPList);
            return 1;
        }
        for(SmartPost sp:SPList){
            if (sp.getCountry().equals(country)) {
                FilteredSPList.add(sp);
            }
        } return 1;
    }

    public void filterResultsByTime (List dOW, int fH, int fM, int tH, int tM) {
            for(SmartPost sp:SPList){
                if (sp.availableOn(dOW,fH,fM,tH,tM)) {
                    FilteredSPList.add(sp);
                }

        }
    }

    public void filterResultsByBoth (String country, List dOW, int fH, int fM, int tH, int tM) {
        if (country.equals("Suomi")) {
            country = "FI";
        } else if (country.equals("Viro")) {
            country = "EE";
        } else {
            country = "BOTH";
        }
        for(SmartPost sp:SPList){
            //System.out.println(sp.getName());
            if (sp.getCountry().equals(country) && sp.availableOn(dOW,fH,fM,tH,tM)) {
                FilteredSPList.add(sp);
            } else if(country.equals("BOTH") && sp.availableOn(dOW,fH,fM,tH,tM)) {
                FilteredSPList.add(sp);
            }
        }
    }

    public void listPosts (){
        for(int i = 0; i < SPList.size(); i++) {
            System.out.println(SPList.get(i).getAll()+"\n-----------\n");
        }
    }

    public ArrayList getList(){
        return SPList;
    }

    public ArrayList getFilteredList() {
        return FilteredSPList;
    }

    public void emptyFilteredList() {
        FilteredSPList.clear();
    }

}
