package com.example.olio_teht;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SmartPost {
    private String id;
    private String name;
    private String city;
    private String address;
    private String country;
    private String postalCode;
    private String availability;
    private String description;
    private ArrayList<Available> AV = new ArrayList<Available>();

    public class Available {
        private int dayOfWeek;
        private int openFromHour;
        private int openFromMinute;
        private int openToHour;
        private int openToMinute;

        public Available (int d, int oFH,int oFM,int oTH, int oTM) {
            dayOfWeek = d;
            openFromHour = oFH;
            openFromMinute = oFM;
            openToHour = oTH;
            openToMinute = oTM;
        }

        public int getDayOfWeek() {
            return dayOfWeek;
        }

        public int getOpenFromHour() {
            return openFromHour;
        }

        public int getOpenFromMinute() {
            return openFromMinute;
        }

        public int getOpenToHour() {
            return openToHour;
        }

        public int getOpenToMinute() {
            return openToMinute;
        }
    }

    public SmartPost (String id, String name, String city, String add, String country, String postal, String availability, String desc) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.address = add;
        this.country = country;
        this.postalCode = postal;
        this.availability = availability;
        this.description = desc;
        this.AV = parseAvailability(availability);
    }

    public ArrayList parseAvailability (String s) {
        ArrayList<Available> AV = new ArrayList<Available>();
        Scanner sc = new Scanner(s);
        int fH, fM,tH,tM;
        while(sc.hasNextLine()) {
            String delims = "[ .–:,;-]+";
            String replace = "kell";
            String str = sc.nextLine();
            str = str.replace(replace, "");
            String[] parsed = str.split(delims);
//            System.out.println("\n---------\n");
//            System.out.println(parsed.length + "-----" + parsed[parsed.length - 1]);
//            for (int i = 0; i < parsed.length; i++) {
//                System.out.println(parsed[i]);
//            }
            try {
                if (parsed[0].equals("24h") || parsed[2].equals("24h")) {
                    for (int i = 1; i <= 7; i++) {
                        fH = 00;
                        fM = 00;
                        tH = 23;
                        tM = 59;
                        AV.add(new Available(i, fH, fM, tH, tM));

                    }
                    continue;
                } else if (parsed[0].equals("ma") || parsed[0].equals("E")) {
                    if (parsed[1].equals("pe") || parsed[1].equals("R")) {
                        for (int i = 1; i <= 5; i++) {
                            fH = Integer.valueOf(parsed[2]);
                            fM = Integer.valueOf(parsed[3]);
                            tH = Integer.valueOf(parsed[4]);
                            tM = Integer.valueOf(parsed[5]);
                            AV.add(new Available(i, fH, fM, tH, tM));

                        }

                        if ((parsed[6].equals("la") && parsed[7].equals("su")) || (parsed[6].equals("L") && parsed[7].equals("P"))) {
                            for (int i = 6; i <= 7; i++) {
                                try {
                                    fH = Integer.valueOf(parsed[8]);
                                    fM = Integer.valueOf(parsed[9]);
                                    tH = Integer.valueOf(parsed[10]);
                                    tM = Integer.valueOf(parsed[11]);
                                    AV.add(new Available(i, fH, fM, tH, tM));

                                } catch (ArrayIndexOutOfBoundsException e) {

                                }
                            }
                        } else if (parsed.length > 7) {
                            fH = Integer.valueOf(parsed[7]);
                            fM = Integer.valueOf(parsed[8]);
                            tH = Integer.valueOf(parsed[9]);
                            tM = Integer.valueOf(parsed[10]);
                            AV.add(new Available(6, fH, fM, tH, tM));

                            fH = Integer.valueOf(parsed[12]);
                            fM = Integer.valueOf(parsed[13]);
                            tH = Integer.valueOf(parsed[14]);
                            tM = Integer.valueOf(parsed[15]);
                            AV.add(new Available(7, fH, fM, tH, tM));

                        } else {
                            continue;
                        }

                    }
                    else if (parsed[5].equals("ti")) {
                        if (parsed[6].equals("ke")){
                            for(int i = 2;i<=3;i++) {
                                fH = Integer.valueOf(parsed[7]);
                                fM = Integer.valueOf(parsed[8]);
                                tH = Integer.valueOf(parsed[9]);
                                tM = Integer.valueOf(parsed[10]);
                                AV.add(new Available(i, fH, fM, tH, tM));
                            }
                            fH = Integer.valueOf(parsed[12]);
                            fM = Integer.valueOf(parsed[13]);
                            tH = Integer.valueOf(parsed[14]);
                            tM = Integer.valueOf(parsed[15]);
                            AV.add(new Available(4, fH, fM, tH, tM));
                            fH = Integer.valueOf(parsed[17]);
                            fM = Integer.valueOf(parsed[18]);
                            tH = Integer.valueOf(parsed[19]);
                            tM = Integer.valueOf(parsed[20]);
                            AV.add(new Available(5, fH, fM, tH, tM));
                            fH = Integer.valueOf(parsed[22]);
                            fM = Integer.valueOf(parsed[23]);
                            tH = Integer.valueOf(parsed[24]);
                            tM = Integer.valueOf(parsed[25]);
                            AV.add(new Available(6, fH, fM, tH, tM));
                        } else if (parsed[6].equals("to")){
                            for(int i = 2;i<=4;i++) {
                                fH = Integer.valueOf(parsed[7]);
                                fM = Integer.valueOf(parsed[8]);
                                tH = Integer.valueOf(parsed[9]);
                                tM = Integer.valueOf(parsed[10]);
                                AV.add(new Available(i, fH, fM, tH, tM));
                            }
                            fH = Integer.valueOf(parsed[12]);
                            fM = Integer.valueOf(parsed[13]);
                            tH = Integer.valueOf(parsed[14]);
                            tM = Integer.valueOf(parsed[15]);
                            AV.add(new Available(5, fH, fM, tH, tM));
                            fH = Integer.valueOf(parsed[17]);
                            fM = Integer.valueOf(parsed[18]);
                            tH = Integer.valueOf(parsed[19]);
                            tM = Integer.valueOf(parsed[20]);
                            AV.add(new Available(6, fH, fM, tH, tM));
                        } else if (parsed[6].equals("pe")) {
                            for(int i = 2;i<=5;i++) {
                                fH = Integer.valueOf(parsed[7]);
                                fM = Integer.valueOf(parsed[8]);
                                tH = Integer.valueOf(parsed[9]);
                                tM = Integer.valueOf(parsed[10]);
                                AV.add(new Available(i, fH, fM, tH, tM));
                            }
                            fH = Integer.valueOf(parsed[12]);
                            fM = Integer.valueOf(parsed[13]);
                            tH = Integer.valueOf(parsed[14]);
                            tM = Integer.valueOf(parsed[15]);
                            AV.add(new Available(6, fH, fM, tH, tM));
                            fH = Integer.valueOf(parsed[17]);
                            fM = Integer.valueOf(parsed[18]);
                            tH = Integer.valueOf(parsed[19]);
                            tM = Integer.valueOf(parsed[20]);
                            AV.add(new Available(7, fH, fM, tH, tM));
                        } else if (parsed[6].equals("la")) {
                            for(int i = 2;i<=6;i++) {
                                fH = Integer.valueOf(parsed[7]);
                                fM = Integer.valueOf(parsed[8]);
                                tH = Integer.valueOf(parsed[9]);
                                tM = Integer.valueOf(parsed[10]);
                                AV.add(new Available(i, fH, fM, tH, tM));
                            }
                            fH = Integer.valueOf(parsed[12]);
                            fM = Integer.valueOf(parsed[13]);
                            tH = Integer.valueOf(parsed[14]);
                            tM = Integer.valueOf(parsed[15]);
                            AV.add(new Available(7, fH, fM, tH, tM));
                        }
                            else {
                            fH = Integer.valueOf(parsed[6]);
                            fM = Integer.valueOf(parsed[7]);
                            tH = Integer.valueOf(parsed[8]);
                            tM = Integer.valueOf(parsed[9]);
                            AV.add(new Available(2, fH, fM, tH, tM));
                            for(int i = 3;i<=4;i++) {
                                fH = Integer.valueOf(parsed[12]);
                                fM = Integer.valueOf(parsed[13]);
                                tH = Integer.valueOf(parsed[14]);
                                tM = Integer.valueOf(parsed[15]);
                                AV.add(new Available(i, fH, fM, tH, tM));
                            }
                            fH = Integer.valueOf(parsed[17]);
                            fM = Integer.valueOf(parsed[18]);
                            tH = Integer.valueOf(parsed[19]);
                            tM = Integer.valueOf(parsed[20]);
                            AV.add(new Available(5, fH, fM, tH, tM));
                            fH = Integer.valueOf(parsed[22]);
                            fM = Integer.valueOf(parsed[23]);
                            tH = Integer.valueOf(parsed[24]);
                            tM = Integer.valueOf(parsed[25]);
                            AV.add(new Available(6, fH, fM, tH, tM));
                        }

                    }
                    else if (parsed[1].equals("la") || parsed[1].equals("L")) {
                        for (int i = 1; i <= 6; i++) {
                            try {
                                fH = Integer.valueOf(parsed[2]);
                                fM = Integer.valueOf(parsed[3]);
                                tH = Integer.valueOf(parsed[4]);
                                tM = Integer.valueOf(parsed[5]);
                                AV.add(new Available(i, fH, fM, tH, tM));
                            } catch (Exception e) {
                                fH = Integer.valueOf(parsed[2]);
                                fM = 00;
                                tH = Integer.valueOf(parsed[3]);
                                tM = 00;
                                AV.add(new Available(i, fH, fM, tH, tM));
                            }
                        }
                        try {
                            fH = Integer.valueOf(parsed[7]);
                            fM = Integer.valueOf(parsed[8]);
                            tH = Integer.valueOf(parsed[9]);
                            tM = Integer.valueOf(parsed[10]);
                            AV.add(new Available(7, fH, fM, tH, tM));

                        } catch (Exception e) {
                            fH = Integer.valueOf(parsed[5]);
                            fM = 00;
                            tH = Integer.valueOf(parsed[6]);
                            tM = 00;
                            AV.add(new Available(7, fH, fM, tH, tM));

                        }

                    } else if (parsed[1].equals("su") || parsed[1].equals("P")) {
                        try {
                            for (int i = 1; i <= 7; i++) {
                                fH = Integer.valueOf(parsed[2]);
                                fM = Integer.valueOf(parsed[3]);
                                tH = Integer.valueOf(parsed[4]);
                                tM = Integer.valueOf(parsed[5]);
                                AV.add(new Available(i, fH, fM, tH, tM));
                            }
                        } catch (ArrayIndexOutOfBoundsException e) {
                            for (int i = 1; i <= 7; i++) {
                                fH = Integer.valueOf(parsed[2]);
                                fM = 00;
                                tH = Integer.valueOf(parsed[3]);
                                tM = 00;
                                AV.add(new Available(i, fH, fM, tH, tM));
                            }
                        }
                        continue;
                    } else if (parsed[1].equals("to")) {
                        for (int i = 1; i <= 4; i++) {
                            fH = Integer.valueOf(parsed[2]);
                            fM = Integer.valueOf(parsed[3]);
                            tH = Integer.valueOf(parsed[4]);
                            tM = Integer.valueOf(parsed[5]);
                            AV.add(new Available(i, fH, fM, tH, tM));

                        }
                        if (parsed[6].equals("pe") && parsed[7].equals("su")) {
                            for (int i = 5; i <= 7; i++) {
                                fH = 00;
                                fM = 00;
                                tH = 00;
                                tM = 00;
                                AV.add(new Available(i, fH, fM, tH, tM));

                            }
                        } else if (parsed[6].equals("pe") && parsed.length <= 11) {
                            fH = Integer.valueOf(parsed[7]);
                            fM = Integer.valueOf(parsed[8]);
                            tH = Integer.valueOf(parsed[9]);
                            tM = Integer.valueOf(parsed[10]);
                            AV.add(new Available(5, fH, fM, tH, tM));
                        } else if (parsed[11].equals("la") && parsed[12].equals("su")) {
                            for (int i = 6; i <= 7; i++) {
                                fH = Integer.valueOf(parsed[13]);
                                fM = Integer.valueOf(parsed[14]);
                                tH = Integer.valueOf(parsed[15]);
                                tM = Integer.valueOf(parsed[16]);
                                AV.add(new Available(i, fH, fM, tH, tM));
                            }
                        } else {
                            fH = Integer.valueOf(parsed[12]);
                            fM = Integer.valueOf(parsed[13]);
                            tH = Integer.valueOf(parsed[14]);
                            tM = Integer.valueOf(parsed[15]);
                            AV.add(new Available(6, fH, fM, tH, tM));

                            fH = Integer.valueOf(parsed[17]);
                            fM = Integer.valueOf(parsed[18]);
                            tH = Integer.valueOf(parsed[19]);
                            tM = Integer.valueOf(parsed[20]);
                            AV.add(new Available(7, fH, fM, tH, tM));

                        }
                    }
                } else if (parsed[0].equals("pe")){
                    fH = Integer.valueOf(parsed[1]);
                    fM = Integer.valueOf(parsed[2]);
                    tH = Integer.valueOf(parsed[3]);
                    tM = Integer.valueOf(parsed[4]);
                    AV.add(new Available(5, fH, fM, tH, tM));
                    fH = Integer.valueOf(parsed[6]);
                    fM = Integer.valueOf(parsed[7]);
                    tH = Integer.valueOf(parsed[8]);
                    tM = Integer.valueOf(parsed[9]);
                    AV.add(new Available(6, fH, fM, tH, tM));
                    fH = Integer.valueOf(parsed[11]);
                    fM = Integer.valueOf(parsed[12]);
                    tH = Integer.valueOf(parsed[13]);
                    tM = Integer.valueOf(parsed[14]);
                    AV.add(new Available(7, fH, fM, tH, tM));
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
        }
        sc.close();
        return AV;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean availableOn(List dayOfWeek, int fromHour, int fromMinute, int toHour, int toMinute){
        LocalTime start = LocalTime.parse(String.format("%02d",fromHour)+":"+String.format("%02d",fromMinute));
        LocalTime finish = LocalTime.parse(String.format("%02d",toHour)+":"+String.format("%02d",toMinute));
//        for(int i=0;i<AV.size();i++){
//            System.out.println(AV.get(i).getDayOfWeek());
//        }
        for(int i = 0; i < dayOfWeek.size();i++) {
            int day = (int) dayOfWeek.get(i);
            try {
            // System.out.println(AV.get(day).getOpenFromHour()+"----"+AV.get(day).getOpenToHour());
            LocalTime openStart = LocalTime.parse(String.format("%02d",AV.get(day).getOpenFromHour())+":"+String.format("%02d",AV.get(day).getOpenFromMinute()));
            LocalTime openFinish = LocalTime.parse(String.format("%02d",AV.get(day).getOpenToHour())+":"+String.format("%02d",AV.get(day).getOpenToMinute()));
                if ((openStart.isBefore(start) || openStart.equals(start)) && (finish.isBefore(openFinish) || finish.equals(openFinish) )) {
                    if (i == dayOfWeek.size() - 1) {
                        return true;
                    }
                    continue;
                } else {
                    return false;
                }
            }catch (IndexOutOfBoundsException e) {
                return false;
            }
        } return true;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getCountry() {
        return country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getDescription() {
        return description;
    }

    public void getAvailability (){
//        for(int i = 0; i<AV.size();i++){
//            System.out.println(AV.get(i).dayOfWeek+"--------------FROM"+AV.get(i).getOpenFromHour()+":"+AV.get(i).getOpenFromMinute()+"---TO---"+AV.get(i).getOpenToHour()+":"+AV.get(i).getOpenToMinute());
//        }
    }

    public String getAll() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n"+id+"\n"+name+"\n"+city+"\n"+address+"\n"+country+"\n"+postalCode+"\n"+availability+"\n"+description);
        return sb.toString();
    }

    public String getAllParsed() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n"+name+"\n\nOsoite:\n"+address+"\n"+postalCode+", "+city+", "+country+"\n\nAvoinna:\n"+availability+"\n\nLisätiedot:\n"+description);
        return sb.toString();
    }
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(city.toUpperCase()+", "+name);
        return sb.toString();
    }

}
