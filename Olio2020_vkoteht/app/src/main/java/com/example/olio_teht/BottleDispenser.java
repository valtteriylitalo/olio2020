package com.example.olio_teht;

import java.util.ArrayList;

public class BottleDispenser {
    private int bottles;
    private float money;
    private ArrayList<Bottle> bottleList = new ArrayList<Bottle>();
    private static BottleDispenser instance = null;

    public BottleDispenser() {
        bottles = 5;
        money = 0.0f;
        // Initialize the array
//		ArrayList<Bottle> bottleList = new ArrayList<Bottle>();
        // Add Bottle-objects to the arraylist
        // Use the default constructor to create new Bottles
        bottleList.add(new Bottle());
        bottleList.add(new Bottle("Pepsi Max", "Pepsi", 0.3, 1.5, 2.2));
        bottleList.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.3, 0.5, 2.0));
        bottleList.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.3, 1.5, 2.5));
        bottleList.add(new Bottle("Fanta Zero", "Fanta", 0.3, 0.5, 1.95));

    }

    public static BottleDispenser getInstance() {
        if (instance == null) {
            instance = new BottleDispenser();
        }
        return instance;
    }

    public void addMoney(int amount) {
        money += amount;
    }

    public int buyBottle(String name, String size) {
        size = size.replace(",",".");
        double sizeNew = Double.parseDouble(size.substring(0,3));
        for(Bottle b : bottleList) {
            if (b.getName().equals(name) && b.getSize() == sizeNew) {
                removeBottle(name,size);
                money -= b.getPrice();
                return 1;
            }
        }
        return 0;
    }

    public void returnMoney() {
        money = 0;
    }

    public int getBottles() {
        return bottles;
    }

    public float getMoney() {
        return money;
    }
    /*
    public void listBottles() {
        for (int i = 0; i < bottles; i++) {
            System.out.println(i + 1 + ". Name: " + bottleList.get(i).getName() + "\n" + "\tSize: "
                    + bottleList.get(i).getSize() + "\tPrice: " + bottleList.get(i).getPrice());

        }
    }
*/
    public void removeBottle(String name, String size) {
        size = size.replace(",",".");
        double sizeNew = Double.parseDouble(size.substring(0,3));
        for(Bottle b : new ArrayList<Bottle>(bottleList)) {
            if (b.getName().equals(name) && b.getSize() == sizeNew) {
                bottleList.remove(b);
            }
    }}

    public double getPrice(String name, String size){
        size = size.replace(",",".");
        double sizeNew = Double.parseDouble(size.substring(0,3));
        for(Bottle b : bottleList) {
            if (b.getName().equals(name) && b.getSize() == sizeNew) {
                return b.getPrice();
            }
        }
        return 0;
    }
}
