package com.example.olio_teht;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        final Button button= findViewById(R.id.L7button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               goToL7();
            }
        });
        final Button button2 = findViewById(R.id.L8button);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToL8();
            }
        });
        final Button button3 = findViewById(R.id.L9button);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToL9();
            }
        });
        final Button button4 = findViewById(R.id.L10button);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToL10();
            }
        });
    }
    public static Toast transitionToast;
    public static String loading = "loading...";

    public void goToL7(){
        Intent intent = new Intent(this, L7.class);
        startActivity(intent);
    }

    public void goToL8(){
        Intent intent = new Intent(this, L8BottleDispenser.class);
        startActivity(intent);
    }

    public void goToL9(){
        transitionToast.makeText(this, loading, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, L9interface.class);
        startActivity(intent);

    }

    public void goToL10(){
        Intent intent = new Intent(this, L10.class);
        startActivity(intent);

    }
}
