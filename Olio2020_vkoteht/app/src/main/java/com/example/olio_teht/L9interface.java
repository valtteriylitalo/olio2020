package com.example.olio_teht;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.InputType;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Calendar;

public class L9interface extends AppCompatActivity {
    private int mHour;
    private int mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_l9interface);
        final SPStorage sp = SPStorage.getInstance();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        mMinute = Calendar.getInstance().get(Calendar.MINUTE);
        final Spinner selection = (Spinner) findViewById(R.id.selection);
        final ArrayAdapter<SmartPost> itemList = new ArrayAdapter<SmartPost>(L9interface.this, android.R.layout.simple_spinner_item, sp.getList());
        selection.setAdapter(itemList);

        final Dialog weekDaySelect;
        final String[] days = {"ma","ti","ke","to","pe","la","su"};
        final ArrayList selectedDays = new ArrayList();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Valitse tarkasteltavat viikonpäivät");

        // initializing a multi-choice alertDialog to select weekdays
        builder.setMultiChoiceItems(days, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            // when a item is clicked it is added to a arraylist and vise versa
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    selectedDays.add(which);
                } else if (selectedDays.contains(which)) {
                    selectedDays.remove(Integer.valueOf(which));
                }
            }
        })
                // positive and negative buttons, on OK click make a string of contents and set it to textView
                .setPositiveButton("OK!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final EditText weekDays = (EditText) findViewById(R.id.weekDays);
                        StringBuilder sb = new StringBuilder();
                        for(int i=0; i < selectedDays.size();i++){
                            sb.append(days[(int)selectedDays.get(i)]+" ");
                        }
                        weekDays.setText(sb.toString());
                    }
                })

                .setNegativeButton("Peruuta", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        weekDaySelect = builder.create();

        // added onclicklistener for the weekdays-field
        final EditText weekDays = (EditText) findViewById(R.id.weekDays);
        weekDays.setInputType(InputType.TYPE_NULL);
        weekDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weekDaySelect.show();
        }
        });

        // same as above but as the focus changes to make sure it works in any case
        weekDays.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    weekDaySelect.show();
                }
            }
        });

        final EditText fromTime = (EditText) findViewById(R.id.fromTime);
        final EditText toTime = (EditText) findViewById(R.id.toTime);

        fromTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(L9interface.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        fromTime.setText(String.format("%02d:%02d",hourOfDay, minute));                    }
                },mHour, mMinute,true);
                timePickerDialog.show();
            }
        });

        toTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(L9interface.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        toTime.setText(String.format("%02d:%02d",hourOfDay, minute));

                    }
                },mHour, mMinute,true);
                timePickerDialog.show();
            }

        });
        final RadioGroup countryGroup = (RadioGroup) findViewById(R.id.countryGroup);

        Button filter = (Button) findViewById(R.id.filterButton);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sp.emptyFilteredList();
                RadioButton r = (RadioButton) findViewById(countryGroup.getCheckedRadioButtonId());
                String country = r.getText().toString();
                if (fromTime.getText().toString().isEmpty() || toTime.getText().toString().isEmpty()) {
                    sp.filterResultsByCountry(country);
                } else {
                    int fH = Integer.parseInt(fromTime.getText().toString().substring(0,2));
                    int fM = Integer.parseInt(fromTime.getText().toString().substring(3,5));
                    int tH = Integer.parseInt(toTime.getText().toString().substring(0,2));
                    int tM = Integer.parseInt(toTime.getText().toString().substring(3,5));
                    sp.filterResultsByBoth(country,selectedDays,fH,fM,tH,tM);
                }
                final Spinner selection2 = (Spinner) findViewById(R.id.selection2);
                final ArrayAdapter<SmartPost> filteredItemList = new ArrayAdapter<SmartPost>(L9interface.this, android.R.layout.simple_spinner_item, sp.getFilteredList());
                selection2.setAdapter(filteredItemList);
            }
        });

        Button getFilteredInfo = (Button) findViewById(R.id.searchButton2);
        getFilteredInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Spinner selection2 = (Spinner) findViewById(R.id.selection2);
                final ArrayAdapter<SmartPost> filteredItemList = new ArrayAdapter<SmartPost>(L9interface.this, android.R.layout.simple_spinner_item, sp.getFilteredList());
                AlertDialog.Builder builder = new AlertDialog.Builder(L9interface.this);
                SmartPost sp = filteredItemList.getItem(selection2.getSelectedItemPosition());
                builder.setTitle("Valitsemasi automaatin tiedot");
                builder.setMessage(sp.getAllParsed());
                builder.create().show();
            }
        });
        Button getInfo = (Button) findViewById(R.id.searchButton);
        getInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(L9interface.this);

                SmartPost sp = itemList.getItem(selection.getSelectedItemPosition());
                builder.setTitle("Valitsemasi automaatin tiedot");
                builder.setMessage(sp.getAllParsed());
                builder.create().show();

            }
        });



    }



}
