package com.example.olio_teht;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class L8BottleDispenser extends AppCompatActivity {



    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_l8_bottle_dispenser);
        final BottleDispenser bd = new BottleDispenser();

        final SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
        TextView tvMin = (TextView) findViewById(R.id.seekBarMin);
        TextView tvMax = (TextView) findViewById(R.id.seekBarMax);
        final TextView tvCurr = (TextView) findViewById(R.id.seekBarCurrent);
        final TextView tvOut = (TextView) findViewById(R.id.outputField);
        final TextView tvMoney = (TextView) findViewById(R.id.moneyField);
        final TextView tvPrice = (TextView) findViewById(R.id.priceField);
        final Spinner spinnerProducts = (Spinner) findViewById(R.id.productSelect);
        final Spinner spinnerSizes = (Spinner) findViewById(R.id.sizeSelect);
        final ArrayList<String> receiptList = new ArrayList<String>();

        tvMin.setText(String.valueOf(seekBar.getMin()));
        tvMax.setText(String.valueOf(seekBar.getMax()));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvCurr.setText("Current choice: "+String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        spinnerProducts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String name = parent.getItemAtPosition(position).toString();
                String size = spinnerSizes.getSelectedItem().toString();
                if(bd.getPrice(name,size)!= 0) {
                    tvPrice.setText("Price of selection: " + bd.getPrice(name, size));
                } else {
                    tvPrice.setText("Selected item not available.");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerSizes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String name = spinnerProducts.getSelectedItem().toString();
                String size = parent.getSelectedItem().toString();
                if(bd.getPrice(name,size)!= 0) {
                    tvPrice.setText("Price of selection: " + bd.getPrice(name, size));
                } else {
                    tvPrice.setText("Selected item not available.");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        Button insertButton = (Button) findViewById(R.id.insertButton);
        insertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvOut.setText("Added "+String.valueOf(seekBar.getProgress())+" coins");
                bd.addMoney(seekBar.getProgress());
                seekBar.setProgress(0);
                tvMoney.setText("Money: "+String.valueOf(bd.getMoney()));

            }
        });

        Button returnButton = (Button) findViewById(R.id.returnButton);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvOut.setText("You got "+bd.getMoney()+" back");
                bd.returnMoney();
                tvMoney.setText("Money: "+String.valueOf(bd.getMoney()));
            }
        });

        Button buyButton = (Button) findViewById(R.id.buyButton);
        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = spinnerProducts.getSelectedItem().toString();
                String size = spinnerSizes.getSelectedItem().toString();
                double purchase = bd.getPrice(name,size);
                if (purchase <= bd.getMoney()){
                    if (bd.buyBottle(name,size)!= 0){
                        tvOut.setText("You purchased "+ name+" size "+size+" for "+purchase);
                        tvMoney.setText(String.valueOf(bd.getMoney()));
                        receiptList.add(name+" "+size+" "+purchase);
                    } else {
                        tvOut.setText("Selected item not available.");
                    }
                } else {
                    tvOut.setText("Add more money!");
                }

            }
        });

        Button receiptButton = (Button) findViewById(R.id.receiptButton);
        receiptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveReceipt(receiptList.get(receiptList.size()-1));
                tvOut.setText("Receipt saved.");
            }
        });


    }

    public void saveReceipt (String writable) {
        try{
            OutputStreamWriter out = new OutputStreamWriter(L8BottleDispenser.this.openFileOutput("ReceiptFile.txt", Context.MODE_PRIVATE));
            out.write("\tRECEIPT\nPurchase\n"+writable);
            out.close();
        } catch (Throwable t) {
            Toast.makeText(this,"Exception"+t.toString(),Toast.LENGTH_LONG).show();
        }
    }
}
