package com.example.olio_teht;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class L7 extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l7);

        final Button buttonL7T2 = findViewById(R.id.buttonL7T2);
        buttonL7T2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView) findViewById(R.id.textViewL7T2);
                tv.setText("Hello World!");
                System.out.println("Hello World!");
            }
        });

        final Button buttonL7T3 = findViewById(R.id.buttonL7T3);
        buttonL7T3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView) findViewById(R.id.textViewL7T3);
                EditText editText = (EditText) findViewById(R.id.textInput);
                String text = editText.getText().toString();
                tv.setText("kirjoitit: " + text);
            }
        });


        EditText edit = (EditText) findViewById(R.id.textInput2);
        edit.addTextChangedListener(new TextWatcher() {
            TextView tv = (TextView) findViewById(R.id.textViewL7T4);

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tv.setText(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        }
        );
        final Button button = findViewById(R.id.textEditorButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToTextEdit();
            }
        });

    }

        public void goToTextEdit(){
            Intent intent = new Intent(this, L7TextEditor.class);
            startActivity(intent);
        }


}
