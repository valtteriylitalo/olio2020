package com.example.olio_teht;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class L7TextEditor extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_l7_text_editor);

        final Button saveFile = findViewById(R.id.button);
        final Button loadFile = findViewById(R.id.button2);
        final EditText editText = (EditText) findViewById(R.id.editText);

        saveFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(L7TextEditor.this);
                builder.setTitle("Valitse tallennettava tiedosto");
                final EditText input = new EditText(L7TextEditor.this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Save(input.getText().toString(), editText.getText().toString());
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });

        loadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(L7TextEditor.this);
                builder.setTitle("Valitse avattava tiedosto");
                final EditText input = new EditText(L7TextEditor.this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            readFile(input.getText().toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });


    }
    public void Save(String fileName, String EditText){
        try{
            OutputStreamWriter out = new OutputStreamWriter(L7TextEditor.this.openFileOutput(fileName, Context.MODE_PRIVATE));
            out.write(EditText);
            out.close();

        } catch (Throwable t) {
            Toast.makeText(this,"Exception"+t.toString(),Toast.LENGTH_LONG).show();
        }
    }

    public void readFile(String i) throws IOException {
        String inputFile = i;
        InputStream is = L7TextEditor.this.openFileInput(inputFile);

        try {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader out = new BufferedReader(isr);
            String outputLine;
            StringBuilder sb = new StringBuilder();

            while ((outputLine = out.readLine()) != null) {
                sb.append(outputLine).append("\n");
            }
            EditText editText = (EditText) findViewById(R.id.editText);
            String content = sb.toString();
            editText.setText(content);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
